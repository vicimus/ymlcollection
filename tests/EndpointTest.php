<?php

namespace Vicimus\YMLCollection\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Vicimus\YMLCollection\YMLCollection;
use Vicimus\YMLCollection\Classes\Endpoint;

/**
 * Test the Endpoint features
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class EndpointTest extends TestCase
{
    /**
     * The directory to use with testing
     *
     * @var string
     */
    const TEST_DIR = __DIR__.'/resources/';

    /**
     * Test constructor
     *
     * @return void
     */
    public function testConstructor()
    {
        $endpoint = new Endpoint;
        $this->assertInstanceOf(Endpoint::class, $endpoint);
    }

    /**
     * This ensures that the duplication bug does not occurr
     *
     * @return void
     */
    public function testDuplicationBug()
    {
        $file = __DIR__.'/resources/example_endpoint.yml';
        $raw = Yaml::parse(file_get_contents($file));
        $unique = [];

        foreach ($raw['paths'] as $path) {
            foreach ($path['operations'] as $verb) {
                $this->assertFalse(in_array($verb['summary'], $unique));
                $unique[] = $verb['summary'];
            }
        }

        $unique = [];
        $endpoint = new Endpoint($raw);
        $this->assertInstanceOf(Endpoint::class, $endpoint);

        foreach ($endpoint->paths as $path) {
            foreach ($path['operations'] as $verb) {
                $this->assertFalse(in_array($verb['summary'], $unique));
                $unique[] = $verb['summary'];
            }
        }
    }
}
