<?php

namespace Vicimus\YMLCollection\Tests;

use PHPUnit\Framework\TestCase;
use Vicimus\YMLCollection\YMLCollection;
use Vicimus\YMLCollection\Classes\Endpoint;
use Vicimus\YMLCollection\Classes\GlobalSettings;
use Illuminate\Support\Collection;

/**
 * Test the YMLCollection features
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class YMLCollectionTest extends TestCase
{
    /**
     * The directory to use with testing
     *
     * @var string
     */
    const TEST_DIR = __DIR__.'/resources/';

    /**
     * Instantiate a new instance
     *
     * @return void
     */
    public function testConstructor()
    {
        $collection = new YMLCollection(self::TEST_DIR);
        $this->assertInstanceOf(YMLCollection::class, $collection);
    }

    /**
     * Ensure initial collection is empty
     *
     * @return void
     */
    public function testCount()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $this->assertEquals(0, $files->count());
    }

    /**
     * Collect the YML files
     *
     * @return void
     */
    public function testCollect()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $this->assertNotEquals(0, $files->collect()->count());
    }

    /**
     * Ensure names can be parsed
     *
     * @return void
     */
    public function testNames()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $this->assertTrue(count($files->names()) > 0);
    }

    /**
     * Test getting a specific endpoint
     *
     * @return void
     */
    public function testGet()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $names = $files->names();

        $endpoints = $files->get($names[0]);
        $this->assertInstanceOf(Endpoint::class, $endpoints[0]);
    }

    /**
     * Test getting a specific endpoint that has a space in it
     *
     * @return void
     */
    public function testGetWithSpace()
    {
        $withSpaces = 'Test Endpoint';
        $files = new YMLCollection(self::TEST_DIR);
        $endpoints = $files->get($withSpaces);

        $this->assertInstanceOf(Endpoint::Class, $endpoints[0]);
    }

    /**
     * Test markdown parsing capabilities
     *
     * @return void
     */
    public function testMarkdown()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $names = $files->names();

        $endpoints = $files->get($names[0]);

        $description = $endpoints[0]->paths['/']['operations']['post']['description'];

        $this->assertNotEquals(strip_tags($description), $description);
    }

    /**
     * Test getting tags from the collection
     *
     * @return void
     */
    public function testGetTags()
    {
        $files = new YMLCollection(self::TEST_DIR);

        $this->assertInternalType('array', $files->tags());
    }

    /**
     * Test getting endpoints from a tag
     *
     * @return void
     */
    public function testGetByTag()
    {
        $files = new YMLCollection(self::TEST_DIR);

        $tags = $files->tags();
        $this->assertTrue(count($tags) > 0);

        $tag = $tags[0];

        $endpoints = $files->getByTag($tag);

        $this->assertInternalType('array', $endpoints);
        $this->assertTrue(count($endpoints) > 0);

        $case = $files->getByTag(strtoupper($tag));

        $this->assertInternalType('array', $case);
        $this->assertTrue(count($case) > 0);
        $this->assertEquals(count($endpoints), count($case));
    }

    /**
     * Test getting global securityDefinitions
     *
     * @return void
     */
    public function testGetSecurityDefinitions()
    {
        $files = new YMLCollection(self::TEST_DIR);

        $security = $files->security();

        $this->assertInternalType('array', $security);
    }

    /**
     * Test getting global schemaDefinitions
     *
     * @return void
     */
    public function testGetSchemaDefinitions()
    {
        $files = new YMLCollection(self::TEST_DIR);

        $schemas = $files->schemas();

        $this->assertInternalType('array', $schemas);
    }

    /**
     * Test global get
     *
     * @return void
     */
    public function testGetGlobal()
    {
        $files = new YMLCollection(self::TEST_DIR);

        $global = $files->getGlobal();

        $this->assertInstanceOf(GlobalSettings::class, $global);
    }

    /**
     * Test gathering all tags
     *
     * @return void
     */
    public function testGetMultipleTags()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $tags = $files->tags();
        $this->assertEquals(6, count($tags));
    }

    /**
     * Test getting opereation by a custom tag inside it
     *
     * @return void
     */
    public function testGetOperationByUniqueTag()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $endpoints = $files->getByTag('Dragon');

        $this->assertEquals(1, count($endpoints));

        $this->assertEquals(1, $endpoints[0]->count());
    }

    /**
     * Test getting the collection of pages (if any)
     *
     * @return void
     */
    public function testPages()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $pages = $files->pages();

        $this->assertInstanceOf(Collection::class, $pages);
        $this->assertEquals(2, count($pages));
    }

    /**
     * Test getting a specific page
     *
     * @return void
     */
    public function testGetPage()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $page = $files->page('/');

        $this->assertNotNull($page);
        $this->assertTrue(stripos($page->content, 'register') !== false);
    }

    /**
     * Test searching pages
     *
     * @return void
     */
    public function testSearch()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $results = $files->search('event tracking');

        $this->assertEquals(1, count($results));
        $this->assertGreaterThan(0, $results[0]->score);
    }

    /**
     * Case shouldnt matter
     *
     * @return void
     */
    public function testCaseInsensitive()
    {
        $files = new YMLCollection(self::TEST_DIR);
        $endpoint = $files->get('Tasks');
        $this->assertGreaterThan(0, count($endpoint));

        $endpoint = $files->get('tasks');
        $this->assertGreaterThan(0, count($endpoint));
    }
}
