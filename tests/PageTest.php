<?php

namespace Vicimus\YMLCollection\Tests;

use PHPUnit\Framework\TestCase;
use Vicimus\YMLCollection\YMLCollection;
use Vicimus\YMLCollection\Classes\Page;

/**
 * Test the Page features
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class PageTest extends TestCase
{
    /**
     * Holds the Collection
     *
     * @var YMLCollection
     */
    protected $collectioni = null;

    /**
     * The directory to use with testing
     *
     * @var string
     */
    const TEST_DIR = __DIR__.'/resources/';

    /**
     * Create a collection
     */
    public function __construct()
    {
        $this->collection = new YMLCollection(self::TEST_DIR);
    }

    /**
     * Test constructor
     *
     * @return void
     */
    public function testConstructor()
    {
        $page = new Page;
        $this->assertInstanceOf(Page::class, $page);
    }

    /**
     * Test getting the first page
     *
     * @return void
     */
    public function testGetPageFromCollection()
    {
        $page = $this->collection->pages()->first();
        $this->assertInstanceOf(Page::class, $page);
    }

    /**
     * Test to ensure there is markup in the content of the page
     *
     * @return void
     */
    public function testGetPageMarkup()
    {
        $page = $this->collection->page('/');
        $this->assertNotEquals(
            $page->content,
            strip_tags($page->content)
        );
    }

    /**
     * Test to ensure the headings were extracted property
     *
     * @return void
     */
    public function testGetHeadings()
    {
        $page = $this->collection->page('/');
        $this->assertEquals(4, count($page->headings));
    }

    /**
     * Test getting sub headings
     *
     * @return void
     */
    public function testSubHeadings()
    {
        $page = $this->collection->page('/');
        $heading = $page->headings[1];
        $this->assertInternalType('array', $heading);
        $this->assertEquals(1, count($heading['sub']));
    }

    /**
     * Test getting a page regardless of case
     *
     * @return void
     */
    public function testGetPageInsensitive()
    {
        $page = $this->collection->page('case/sensitivity');
        $this->assertInstanceOf(Page::class, $page);

        $page = $this->collection->page('CASE/SenSitiVity');
        $this->assertInstanceOf(Page::class, $page);
    }
}
