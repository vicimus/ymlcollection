<?php

namespace Vicimus\YMLCollection;

use Parsedown;
use Symfony\Component\Yaml\Yaml;
use Illuminate\Support\Collection;
use Symfony\Component\Finder\Finder;
use Vicimus\YMLCollection\Classes\Footer;
use Vicimus\YMLCollection\Classes\Endpoint;
use Vicimus\YMLCollection\Factories\PageFactory;
use Vicimus\YMLCollection\Factories\GlobalFactory;
use Vicimus\YMLCollection\Exceptions\ParseException;
use Symfony\Component\Yaml\Exception\ParseException as YamlParseException;

/**
 * Collects the YML files of the API and allows various information to be
 * requested.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class YMLCollection
{
    /**
     * The index of the endpoints name
     *
     * @var string
     */
    const ENDPOINT_NAME = 'name';

    /**
     * The name of the master global scope fil
     *
     * @var string
     */
    const MASTER_FILE = 'global.yml';

    /**
     * The name of the footer file
     *
     * @var string
     */
    const FOOTER_FILE = 'footer.yml';

    /**
     * Default payh of yml files
     *
     * @var string
     */
    const DEFAULT_PATH = __DIR__.'/../../../../yml/';

    /**
     * The path to use if no path is specified with collect
     *
     * @var string
     */
    protected $path = null;

    /**
     * Stores files found during collection
     *
     * @var string[]
     */
    protected $files = [];

    /**
     * Stores markdown files found during collection
     *
     * @var string[]
     */
    protected $markdowns = [];

    /**
     * Cache the names of all endpoints
     *
     * @var string[]
     */
    protected $names = [];

    /**
     * Cache the contents of all the yml files
     *
     * @var mixed[]
     */
    protected $endpoints = [];

    /**
     * Cache the contents of the page files
     *
     * @var Collection
     */
    protected $pages = [];

    /**
     * Cache the tags from the yml files
     *
     * @var string[]
     */
    protected $tags = [];

    /**
     * Path to the master file (if present)
     *
     * @var string
     */
    protected $master = null;

    /**
     * Get a container object
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Global information that may or may not be available through the inclusion
     * of a kakuna.yml file in the yml directory.
     *
     * @var stdClass
     */
    protected $global;

    /**
     * Indicates if the files have been parsed or not
     *
     * @var bool
     */
    protected $parsed = false;

    /**
     * Holds the footer info that is parsed from footer.yml
     *
     * @var array
     */
    protected $footer = null;

    /**
     * Instantiate a new instance
     *
     * @param string $path OPTIONAL You can specify a custom directory
     */
    public function __construct($path = null)
    {
        $this->path = $path;
        if (is_null($this->path)) {
            $this->path = self::DEFAULT_PATH;
        }

        $this->pages = new Collection;
        $this->global = $this->container(GlobalFactory::class)->make([]);
        $this->footer = new Footer;
    }

    /**
     * Parse the directory for YML files and add them to the collection
     *
     * @param string $path OPTIONAL You can specify an alternative location
     *
     * @return YMLCollection
     */
    public function collect($path = null)
    {
        if (is_null($path)) {
            $path = $this->path;
        }

        if (!$path) {
            $this->global->issue('nopath');
            return $this;
        }

        $finder = new Finder;
        $results = $finder->files()->in($path)
                          ->name('*.yml')
                          ->notName(self::MASTER_FILE)
                          ->notName(self::FOOTER_FILE);

        foreach ($results as $file) {
            $this->files[] = $file->getRealPath();
        }

        $finder = new Finder;
        $kakuna = $finder->files()->in($path)->name(self::MASTER_FILE);

        if (count($kakuna)) {
            foreach ($kakuna as $file) {
                $this->master = $file->getRealPath();
                break;
            }
        }

        $footerData = null;
        $finder = new Finder;
        $footerPath = $finder->files()->in($path)->name(self::FOOTER_FILE);
        if (count($footerPath)) {
            foreach ($footerPath as $foot) {
                $footerData = $foot->getRealPath();
            }
        }

        if ($footerData) {
            $footer = Yaml::parse(file_get_contents($footerData));
            if (isset($footer['include'])) {
                try {
                    $footer = json_decode(
                        file_get_contents($footer['include']),
                        true
                    );
                } catch (\ErrorException $err) {
                    $footer = [];
                }
            }

            $this->footer = new Footer($footer);
        }

        return $this;
    }

    /**
     * Get all endpoints
     *
     * @return mixed[]
     */
    public function endpoints()
    {
        if (!count($this->endpoints)) {
            $this->parse();
        }

        return $this->endpoints;
    }

    /**
     * Get the page collection
     *
     * @return mixed[]
     */
    public function pages()
    {
        if (!count($this->pages)) {
            $this->parse();
        }

        return $this->pages;
    }

    /**
     * Count the number of files that have been collected
     *
     * @return integer
     */
    public function count()
    {
        return count($this->files);
    }

    /**
     * Get the names of all endpoints
     *
     * @return string[]
     */
    public function names()
    {
        if (count($this->names)) {
            return $this->names;
        }

        if (!count($this->endpoints)) {
            $this->parse();
        }

        $names = [];
        foreach ($this->endpoints as $endpoint) {
            if (!array_key_exists(self::ENDPOINT_NAME, $endpoint)) {
                continue;
            }

            if (!in_array($endpoint->name, $names)) {
                $names[] = $endpoint->name;
            }
        }

        usort($names, function ($a, $b) {
            return strcmp($a, $b);
        });

        $this->names = $names;
        return $this->names;
    }

    /**
     * Parse the collection of YML files
     *
     * @return YMLCollection
     */
    public function parse()
    {
        if ($this->parsed) {
            return $this;
        }
        if (!count($this->files)) {
            $this->collect();
        }

        if (!count($this->files)) {
            $this->global->empty = true;
            return $this;
        }

        if ($this->master) {
            $this->global = Yaml::parse(file_get_contents($this->master));

            if ($this->global) {
                $this->markdownifyDescriptions($this->global);
            }

            $this->global = $this->container(GlobalFactory::class)->make($this->global);
        }

        $merges = [];
        foreach ($this->files as $file) {
            try {
                $endpoint = Yaml::parse(file_get_contents($file));
            } catch (YamlParseException $ex) {
                $error = substr($file, stripos($file, '/yml')+1);
                throw new ParseException($error, $ex->getMessage());
            }

            if (!$endpoint) {
                continue;
            }

            if (array_key_exists('type', $endpoint) && $endpoint['type'] == 'page') {
                if (!array_key_exists('priority', $endpoint)) {
                    $endpoint['priority'] = 5;
                }

                if (array_key_exists('extends', $endpoint)) {
                    $merges[] = $endpoint;
                    continue;
                }

                $this->pages->push($this->container(PageFactory::class)->make(
                    $this->global,
                    $endpoint
                ));
            } else {
                $endpointData = $this->markdownify($endpoint);
                $this->endpoints[] = new Endpoint($endpointData);
            }
        }

        $this->doPageMerges($merges);

        if (!count($this->files)) {
            $this->global->issue('nofiles');
            $this->global->empty = true;
        }

        $this->parsed = true;
        return $this;
    }

    /**
     * Merges extended pages into their parent page
     *
     * @throws ParseException
     * @param array $merges The pages that need to be merged
     *
     * @return void
     */
    protected function doPageMerges(array $merges)
    {
        foreach ($merges as $child) {
            $parent = $this->page($child['extends']);
            if (!$parent) {
                throw new ParseException(
                    'Page set to extend a non-existent page'
                );
            }

            $parent->merge(
                $this->container(PageFactory::class)->make(
                    $this->global,
                    $child
                )
            );
        }
    }

    /**
     * Parse any possible markdown in specific
     *
     * @param array $endpoint The endpoint information parsed from YAML
     *
     * @return array
     */
    protected function markdownify(array $endpoint)
    {
        $parser = new Parsedown;
        if (!isset($endpoint['paths']) || !is_array($endpoint['paths'])) {
            return $endpoint;
        }

        foreach ($endpoint['paths'] as &$path) {
            if (!is_array($path)) {
                continue;
            }

            if (!array_key_exists('operations', $path) || !is_array($path['operations'])) {
                continue;
            }

            foreach ($path['operations'] as &$verb) {
                if (!$verb) {
                    continue;
                }

                if (array_key_exists('description', $verb)) {
                    $verb['description'] = $parser->text($verb['description']);
                }

                if (array_key_exists('parameters', $verb)) {
                    if (!is_array($verb['parameters'])) {
                        $verb['parameters'] = [];
                    }

                    foreach ($verb['parameters'] as &$parameter) {
                        if (!$parameter) {
                            continue;
                        }

                        if (array_key_exists('description', $parameter)) {
                            $desc = $parameter['description'];
                            $text = $parser->text($desc);
                            $parameter['description'] = $text;
                        }
                    }

                    $params = [];
                    foreach ($verb['parameters'] as $index => $par) {
                        if ($par && $this->isValidParameter($par)) {
                            $params[] = $par;
                        }
                    }
                    $verb['parameters'] = $params;
                }

                if (array_key_exists('responses', $verb) && is_array($verb['responses'])) {
                    foreach ($verb['responses'] as &$response) {
                        if (!is_array($response)) {
                            continue;
                        }
                        if (is_array($response) && array_key_exists(
                            'description',
                            $response
                        )) {
                            $desc = $response['description'];
                            $text = $parser->text($desc);
                            $response['description'] = $text;
                        }
                    }
                }
            }
        }

        return $endpoint;
    }

    /**
     * Check if a parameter is valid or not
     *
     * @param array $parameter The parameter to inspect
     *
     * @return bool
     */
    protected function isValidParameter(array $parameter)
    {
        return array_key_exists('name', $parameter);
    }

    /**
     * Get an endpoint by name
     *
     * @param string $name The name of the endpoint to get
     *
     * @return \stdClass
     */
    public function get($name)
    {
        if (!count($this->endpoints)) {
            $this->parse();
        }

        $matches = [];
        foreach ($this->endpoints as $endpoint) {
            if (strtolower($endpoint->name) == strtolower($name)) {
                $matches[] = $endpoint;
            }
        }

        return $matches;
    }

    /**
     * Get a page from within the collection
     *
     * @param string $url The url of the page to get
     *
     * @return \stdClass
     */
    public function page($url)
    {
        if (!$this->pages->count()) {
            $this->parse();
        }

        foreach ($this->pages as $page) {
            if (strtolower($page->url) == strtolower($url)) {
                return $this->container(PageFactory::class)->prepare($page);
            }
        }

        return null;
    }

    /**
     * Search for a term
     *
     * @param string $term The term to search for
     *
     * @return array
     */
    public function search($term)
    {
        if (!$this->pages->count()) {
            $this->parse();
        }

        $results = $this->searchPages($term);
        $results = array_merge($results, $this->searchEndpoints($term));

        usort($results, function ($a, $b) {
            if ($a->score === $b->score) {
                return 0;
            }

            return $a->score < $b->score ? 1 : -1;
        });

        return $results;
    }

    /**
     * Search pages for a term
     *
     * @param string $term The term to search for
     *
     * @return array
     */
    protected function searchPages($term)
    {
        $results = [];
        foreach ($this->pages as $page) {
            $score = 0;
            $loaded = $this->container(PageFactory::class)->prepare($page);
            $match = $page->matchContent($term);
            $score = $match;
            $headings = $page->matchHeadings($term);
            $match += $headings;
            $score += $headings * 10;

            $tags = $page->matchTags($term);
            $match += $tags;
            $score += $tags * 20;

            if ($match) {
                $loaded->score = $score;
                $loaded->matches = $match;
                $results[] = $loaded;
            }
        }

        return $results;
    }

    /**
     * Search endpoints for a term
     *
     * @param string $term The term to search for
     *
     * @return array
     */
    protected function searchEndpoints($term)
    {
        if (!count($this->endpoints)) {
            $this->parse();
        }

        $term = strtolower($term);
        $results = [];
        foreach ($this->endpoints as $endpoint) {
            $tags = array_values(array_unique($endpoint->getTags()));
            array_walk($tags, function (&$value) {
                $value = strtolower($value);
            });

            $score = 0;
            $match = 0;
            if (in_array($term, $tags)) {
                $score = 15;
                $match = 1;
            } else {
                foreach ($tags as $tag) {
                    if ($count = substr_count($tag, $term)) {
                        $score += $count;
                        $match += $count;
                    }
                }
            }

            $matches = $endpoint->match($term);
            $score += $matches;
            $match += $matches;

            if ($score) {
                $endpoint->score = $score;
                $endpoint->matches = $match;
                $results[] = $endpoint;
            }
        }

        return $results;
    }

    /**
     * Get a list of unique tags within the collection of endpoints
     *
     * @return string[]
     */
    public function tags()
    {
        if (!count($this->endpoints)) {
            $this->parse();
        }

        $tags = [];
        foreach ($this->endpoints as $endpoint) {
            $tags = array_merge($tags, $endpoint->getTags());
        }

        return array_values(array_unique($tags));
    }

    /**
     * Looks into a path structure to see if any of the operations
     * have custom tags.
     *
     * @param array $path The path structure to check
     *
     * @return array
     */
    protected function getPathTags(array &$path)
    {
        $tags = [];
        foreach ($path['operations'] as $operation) {
            if (array_key_exists('tags', $operation)) {
                foreach ($operation['tags'] as $tag) {
                    $tags[] = $tag;
                }
            }
        }

        return $tags;
    }

    /**
     * Get the footer, if available
     *
     * @return array
     */
    public function getFooter()
    {
        if (!count($this->endpoints)) {
            $this->parse();
        }

        $this->collect();

        return $this->footer;
    }

    /**
     * Get details of the pages without sending all the content
     *
     * @return array[]
     */
    public function getPageInfo()
    {
        $info = [];
        foreach ($this->pages() as $page) {
            if ($this->global->menuHeadings) {
                $page = app(PageFactory::class)->prepare($page);
                $page->headings = app(PageFactory::class)->headings($page);
            }

            $payload = [
                'name'     => $page->name,
                'url'      => $page->url,
                'priority' => $page->priority,
                'headings' => $page->headings,
            ];

            if (substr($payload['url'], 0, 1) == '/') {
                $payload['url'] = substr($payload['url'], 1);
            }

            $info[] = $payload;
        }

        usort($info, function ($a, $b) {
            if ($a['priority'] == $b['priority']) {
                return strcmp($a['name'], $b['name']);
            }

            return $a['priority'] > $b['priority'] ? 1 : -1;
        });

        return $info;
    }

    /**
     * Get a collection of endpoints based on a specific tag
     *
     * @todo Refactor into multiple methods
     *
     * @param string $tag The tag to search for
     *
     * @return string[]
     */
    public function getByTag($tag)
    {
        $return = new Endpoint;

        if (!count($this->endpoints)) {
            $this->parse();
        }

        foreach ($this->endpoints as $endpoint) {
            foreach ($endpoint->paths as $url => $path) {
                $absolute = $endpoint->path.$url;

                foreach ($path['operations'] as $verb => $operation) {
                    $match = in_array($tag, $endpoint->tags);

                    if (array_key_exists('tags', $operation)) {
                        if (in_array(strtolower($tag), array_map(function ($value) {
                            return strtolower($value);
                        }, $operation['tags']))) {
                            $match = true;
                        }
                    }

                    if (array_key_exists('tags', $operation)) {
                        if (in_array('!'.$tag, $operation['tags'])) {
                            $match = false;
                        }
                    }

                    if ($match) {
                        $return->tags[] = $tag;
                        $return->addOperation($absolute, $verb, $operation);
                    }
                }
            }
        }

        $return->tags = array_unique($return->tags);
        return [$return];
    }

    /**
     * Get the global information
     *
     * @return stdClass
     */
    public function getGlobal()
    {
        if (!$this->parsed) {
            $this->parse();
        }

        return $this->global;
    }

    /**
     * Will recursively search for descriptions to markdownify
     *
     * @param array $properties Properties of Global
     *
     * @return void
     */
    protected function markdownifyDescriptions(array &$properties)
    {
        $parser = new Parsedown;
        foreach ($properties as $index => &$property) {
            if (is_array($property)) {
                $this->markdownifyDescriptions($property);
            } elseif ($index == 'description') {
                $property = addslashes(str_replace(
                    "\n",
                    '',
                    $parser->text($property)
                ));
            }
        }
    }

    /**
     * Get an array of security definitions present in the global scope
     *
     * @return stdClass[]
     */
    public function security()
    {
        if (!$this->global) {
            $this->parse();
        }

        if (!$this->global) {
            return [];
        }

        if (array_key_exists('securityDefinitions', $this->global)) {
            return $this->global['securityDefinitions'];
        }

        return [];
    }

    /**
     * Get an array of schema definitions present in the global scope
     *
     * @return stdClass[]
     */
    public function schemas()
    {
        if (!$this->global) {
            $this->parse();
        }

        if (!$this->global) {
            return [];
        }

        if (property_exists($this->global, 'schemaDefinitions')) {
            return $this->global->schemaDefinitions;
        }

        return [];
    }

    /**
     * Get an instance of a class
     *
     * @param string $class The class to get
     *
     * @return mixed
     */
    protected function container($class)
    {
        if (!array_key_exists($class, $this->container)) {
            return new $class;
        }

        return $this->container[$class];
    }
}
