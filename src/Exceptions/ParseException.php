<?php

namespace Vicimus\YMLCollection\Exceptions;

/**
 * Class ParseException
 *
 * Thrown when encountering an error during parsing of endpoints or pages
 *
 * @package Vicimus\YMLCollection
 */
class ParseException extends \Exception
{
    protected $file;

    /**
     * Construct the instance of the exception
     *
     * @param string    $file     The file throwing the exception
     * @param string    $message  The message to display
     * @param int       $code     The code
     * @param Exception $previous The previous exception
     */
    public function __construct(
        $file,
        $message,
        $code = 0,
        Exception $previous = null
    ) {
        $this->file = $file;

        parent::__construct($message, $code, $previous);
    }
}
