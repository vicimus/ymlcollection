<?php

namespace Vicimus\YMLCollection\Factories;

use Parsedown;
use Vicimus\YMLCollection\Classes\Page;
use Symfony\Component\DomCrawler\Crawler;
use Vicimus\YMLCollection\Classes\GlobalSettings;

/**
 * Creates and manipulates Page instances
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class PageFactory
{
    /**
     * Create a new page from a set of data
     *
     * @param GlobalSettings $global The global settings
     * @param array          $data   The data read from the YML collection
     *
     * @return Page
     */
    public function make(GlobalSettings $global, array $data = array())
    {
        $page = new Page;

        $page->showHeadings = $global->showHeadings;

        foreach ($data as $property => $value) {
            $page->$property = $value;
        }

        return $page;
    }

    /**
     * Converts the fresh page instance into the version necessary
     * to display its information.
     *
     * @param Page $page The page to prepare
     *
     * @return Page
     */
    public function prepare(Page $page)
    {
        $parser = new Parsedown;

        $page->intro = $parser->text($page->intro);

        $page->content = $parser->text($page->content);
        $page->content = str_replace(
            '<table>',
            '<table class="ui compact celled striped table">',
            $page->content
        );

        $page->content = str_replace(
            '<blockquote>',
            '<blockquote class="ui icon info message">
                <i class="idea icon"></i>
                <div class="content">',
            $page->content
        );

        $page->content = str_replace(
            '</blockquote>',
            '</div></blockquote>',
            $page->content
        );

        $page->content = str_replace(
            '<h4>',
            '<h4 class="ui info header">',
            $page->content
        );

        $page->headings = $this->headings($page);
        return $page;
    }

    /**
     * Returns an array of headings found within page content
     *
     * @param Page $page The page to look for headings in
     *
     * @return array[]
     */
    public function headings(Page $page)
    {
        $headings = [];

        $crawler = new Crawler($page->content);
        $heading = [];

        $crawler = $crawler->filter('h2,h3');
        foreach ($crawler as $node) {
            if ($node->localName == 'h2') {
                if (array_key_exists('title', $heading)) {
                    $headings[] = $heading;
                    $heading = [];
                }

                $heading['title'] = $node->textContent;
                $heading['sub'] = [];
            }

            if ($node->localName == 'h3') {
                $heading['sub'][] = $node->textContent;
            }
        }

        $headings[] = $heading;
        return $headings;
    }
}
