<?php

namespace Vicimus\YMLCollection\Factories;

use Vicimus\YMLCollection\Classes\GlobalSettings;

/**
 * Handles constructing new instances of Global Settings
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class GlobalFactory
{
    /**
     * Create a new instance of Global Settings
     *
     * @param array $data OPTIONAL The data from the parser
     *
     * @return GlobalSettings
     */
    public function make($data = null)
    {
        if (!is_array($data)) {
            $data = [];
        }

        $global = new GlobalSettings;

        foreach ($data as $property => $value) {
            $global->$property = $value;
        }

        if (!count($data)) {
            $global->empty = false;
        }

        return $global;
    }
}
