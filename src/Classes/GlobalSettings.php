<?php

namespace Vicimus\YMLCollection\Classes;

/**
 * Represents global settings of your documentation
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class GlobalSettings
{
    /**
     * The name of the Documentation
     *
     * @var string
     */
    public $name = 'YAMLDocs';

    /**
     * An html version of the name for the documentation
     *
     * @var string
     */
    public $title = '<strong>YAML</strong>Docs';

    /**
     * A description to summarize what your documentation is for
     *
     * @var string
     */
    public $description = '';

    /**
     * The URL to the repo where your documentation is hosted
     *
     * @var string
     */
    public $repo = '';

    /**
     * Indicates there is no documentation. This automatically gets toggled
     * when global info is read in
     *
     * @var bool
     */
    public $empty = true;

    /**
     * The URL to the live API
     *
     * @var string
     */
    public $url = 'http://api.example.app';

    /**
     * The version of the application
     *
     * @var string
     */
    public $version = '';

    /**
     * Are tags visible in the menu?
     *
     * @var sring
     */
    public $tags = 'on';

    /**
     * Which theme to use
     *
     * @var string
     */
    public $theme = 'semantic-contained';

    /**
     * Show headings at the top of pages
     *
     * @var string
     */
    public $showHeadings = 'on';

    /**
     * Fixed menu but only useful for shorter menus
     *
     * @var string
     */
    public $menuSticky = false;

    /**
     * Show page headings in the navigation
     *
     * @var bool
     */
    public $menuHeadings = true;

    /**
     * Collapse the menu and only show the headings
     *
     * @var bool
     */
    public $menuCollapse = false;

    /**
     * Determines if heading should be shown on endpoint pages
     *
     * @var string
     */
    public $endpointHeaders = 'on';

    /**
     * Protects documentation with Onyx Auth
     *
     * @var bool
     */
    public $auth = false;

    /**
     * Stores all issues encountered during parsing
     *
     * @var string[]
     */
    protected $issues = [];

    /**
     * Check if the global has an issue
     *
     * @param string $slug The slug to check for
     *
     * @return bool
     */
    public function hasIssue($slug)
    {
        return in_array($slug, $this->issues);
    }

    /**
     * Get all issues
     *
     * @return array
     */
    public function issues()
    {
        return array_unique($this->issues);
    }

    /**
     * Add an issue to the global so it can be referenced later
     *
     * @param string $slug The slug representing the issue
     *
     * @return GlobalSettings
     */
    public function issue($slug)
    {
        $this->issues[] = $slug;
        return $this;
    }

    /**
     * Check if the menu should be collapsed or not
     *
     * @param array $pages An array of pages
     *
     * @return bool
     */
    public function isMenuCollapsed($pages)
    {
        if ($this->menuCollapse === false) {
            return false;
        }

        if ($this->menuCollapse === true) {
            return true;
        }

        $total = 0;
        foreach ($pages as $page) {
            $total += count($page['headings']);
        }

        return $this->menuCollapse < $total;
    }
}
