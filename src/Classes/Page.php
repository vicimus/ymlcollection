<?php

namespace Vicimus\YMLCollection\Classes;

/**
 * Represents a Page from the documentation
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class Page
{
    /**
     * The name of the page
     *
     * @var string
     */
    public $name;

    /**
     * Indicates if the top header should be shown
     */
    public $showHeadings = true;

    /**
     * The URL
     *
     * @var string
     */
    public $url;

    /**
     * Where it should appear in the links
     *
     * @var integer
     */
    public $priority = 5;

    /**
     * Optionally, a page can supply a custom header otherwise the
     * name of the page will be used.
     *
     * @var string
     */
    public $heading = null;

    /**
     * Show the intro on the page before all other content
     *
     * @var bool
     */
    public $showIntro = true;

    /**
     * The type of information contained in this class
     *
     * @var string
     */
    public $type = 'page';

    /**
     * Any tags that are assigned to the page
     *
     * @var string[]
     */
    public $tags = [];

    /**
     * Stores the extracted Headings
     *
     * @var array[]
     */
    public $headings = [];

    /**
     * Some pages can offer an intro to be used as a brief description of the
     * topics the page covers. Used in search results
     *
     * @var string
     */
    public $intro = null;

    /**
     * The main content of the page
     *
     * @var string
     */
    public $content = '';

    /**
     * Match a search term
     *
     * @param string $term The term to search for
     *
     * @return int
     */
    public function matchContent($term)
    {
        $intro = substr_count(strtolower($this->intro), strtolower($term));
        return $intro + substr_count(strtolower($this->content), strtolower($term));
    }

    /**
     * Find a search term in a heading
     *
     * @param string $term The term to search for
     *
     * @return int
     */
    public function matchHeadings($term)
    {
        $score = 0;
        foreach ($this->headings as $heading) {
            $score += substr_count(
                strtolower($heading['title']),
                strtolower($term)
            );
        }

        return $score;
    }

    /**
     * Find a search term in tags
     *
     * @param string $term The term to search for
     *
     * @return int
     */
    public function matchTags($term)
    {
        $score = 0;
        foreach ($this->tags as $tag) {
            if ($term == $tag) {
                $score++;
            }
        }

        return $score;
    }

    /**
     * Merge a child page into this existing page
     *
     * @param Page $child The child page to merge
     *
     * @return void
     */
    public function merge(Page $child)
    {
        $this->content .= PHP_EOL.PHP_EOL.$child->content;
    }
}
