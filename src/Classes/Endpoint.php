<?php

namespace Vicimus\YMLCollection\Classes;

use Vicimus\YMLCollection\Exceptions\ParseException;

/**
 * Represents a collection of paths and operations
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
class Endpoint
{
    /**
     * Endpoints can contain tags that are applied to all operations
     * within them
     *
     * @var string[]
     */
    public $tags = [];

    /**
     * Declares what type of information this contains
     *
     * @var string
     */
    public $type = 'endpoint';

    /**
     * Endpoints operations are grouped by path
     *
     * @var array[]
     */
    public $paths = [];

    /**
     * The base path for this endpoint
     *
     * @var string
     */
    public $path = '';

    /**
     * The URL used to reach the information in this endpoint
     *
     * @var string
     */
    public $url;

    /**
     * A name for the endpoint
     *
     * @var string
     */
    public $name = '';

    /**
     * Paths and operations can have categories
     *
     * @var string[]
     */
    public $categories = [];

    /**
     * Create a new instance from data extracted from yml documents
     *
     * @param array $data The data read from yml
     */
    public function __construct(array $data = array())
    {
        foreach ($data as $property => $value) {
            $this->$property = $value;
        }

        $unique = [];
        $operations = [];
        if (!isset($this->paths) || !is_array($this->paths)) {
            $this->paths = [];
        }

        foreach ($this->paths as &$path) {
            if (!$path || gettype($path) === 'string') {
                $message = $path;
                continue;
            }

            if (!isset($path['operations']) || !is_array($path['operations'])) {
                $path['operations'] = [];
                continue;
            }

            foreach ($path['operations'] as $verb => $operation) {
                $operation['verb'] = $verb;
                if (array_key_exists('security', $operation) && !is_array($operation['security'])) {
                    $operation['security'] = [$operation['security']];
                }

                $operations[] = $operation;
            }

            if (isset($path['category'])) {
                $unique[$path['category']] = $path['category'];
            }
        }

        $this->operations = $operations;

        $this->categories = array_values($unique);
        $master = [];

        foreach ($this->categories as $category) {
            $master[$category] = [];
            foreach ($this->paths as $pathNode) {
                if (isset($pathNode['category']) && $pathNode['category'] == $category) {
                    foreach ($pathNode['operations'] as $op) {
                        if (!$op) {
                            continue;
                        }

                        $master[$category][] = [
                            'summary' => $op['summary'],
                            'index'   => $this->getOperationIndex($op),
                        ];
                    }
                }
            }
        }

        $this->categories = $master;
        $this->url = 'api/'.$this->name;
    }

    /**
     * Find a term within all of the endpoint fields and return the number
     * of times it was found
     *
     * @param string $term The term to search for
     *
     * @return int
     */
    public function match($term)
    {
        $count = 0;
        foreach ($this->operations as $operation) {
            if ($operation['description']) {
                $count += substr_count($operation['description'], $term);
            }

            if (isset($operation['parameters'])) {
                foreach ($operation['parameters'] as $param) {
                    $count += substr_count($param['description'], $term);
                }
            }
        }
        return $count;
    }

    /**
     * Find an operations index in the main operations array
     *
     * @param array $check The operation to check
     *
     * @return int
     */
    protected function getOperationIndex(array $check)
    {
        $defaults = [
            'summary' => null,
            'description' => null,
            'responses' => null,
        ];

        $check = array_merge($defaults, $check);

        foreach ($this->operations as $index => $operation) {
            $operation = array_merge($defaults, $operation);

            if ($operation['summary'] != $check['summary']) {
                continue;
            }

            if ($operation['description'] != $check['description']) {
                continue;
            }

            if (count($operation['responses']) != count($check['responses'])) {
                continue;
            }

            return $index;
        }

        return -1;
    }

    /**
     * Add a new operation to the endpoint
     *
     * @param string $path      The path for the operation
     * @param string $verb      The verb to use with the operation
     * @param array  $operation The operation data
     *
     * @return Endpoint
     */
    public function addOperation($path, $verb, array $operation)
    {
        $this->paths[$path]['operations'][$verb] = $operation;
        return $this;
    }

    /**
     * Look through all operations for tags
     *
     * @todo Refactor this ugly ass thing
     *
     * @return string[]
     */
    public function getTags()
    {
        $tags = [];
        foreach ($this->paths as $path) {
            if (!isset($path['operations']) || !is_array($path['operations'])) {
                continue;
            }

            foreach ($path['operations'] as $operation) {
                if (!is_array($operation)) {
                    continue;
                }

                if (array_key_exists('tags', $operation)) {
                    $matches = [];
                    foreach ($operation['tags'] as $tag) {
                        if (substr($tag, 0, 1) !== '!') {
                            $matches[] = $tag;
                        }
                    }

                    $tags = array_merge($tags, $matches);
                }
            }
        }

        return array_merge($this->tags, $tags);
    }

    /**
     * Count the number of operations in the endpoint
     *
     * @return int
     */
    public function count()
    {
        $total = 0;
        foreach ($this->paths as $path) {
            $total += count($path);
        }

        return $total;
    }
}
