<?php

namespace Vicimus\YMLCollection\Classes;

/**
 * Represents a Footer
 */
class Footer implements \JsonSerializable
{
    /**
     * The links to display in the footer
     *
     * @var array
     */
    protected $links = [];

    /**
     * Construct with data parsed from yml files
     *
     * @param array $data Data parsed from footer.yml
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $property => $value) {
            $this->$property = $value;
        }
    }

    /**
     * Get links
     *
     * @return array
     */
    public function links()
    {
        if (!is_array($this->links)) {
            return [];
        }

        return $this->links;
    }

    /**
     * Convert into a json object
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'links' => $this->links,
        ];
    }
}
